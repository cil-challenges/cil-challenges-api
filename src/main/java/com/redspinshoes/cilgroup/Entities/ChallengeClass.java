package com.redspinshoes.cilgroup.Entities;

import javax.persistence.*;

@Entity
public class ChallengeClass {
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer classId;
    private String completionDate;
    private String instructorName;
    private String classType;

    public ChallengeClass() {
    }

    public ChallengeClass(String instructorName, String classType) {
        this.instructorName = instructorName;
        this.classType = classType;
    }

    public Integer getClassId() {
        return classId;
    }

    public void setClassId(Integer classId) {
        this.classId = classId;
    }

    public String getCompletionDate() {
        return completionDate;
    }

    public void setCompletionDate(String completionDate) {
        this.completionDate = completionDate;
    }

    public String getInstructorName() {
        return instructorName;
    }

    public void setInstructorName(String instructorName) {
        this.instructorName = instructorName;
    }

    public String getClassType() {
        return classType;
    }

    public void setClassType(String classType) {
        this.classType = classType;
    }
}
