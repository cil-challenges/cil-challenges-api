package com.redspinshoes.cilgroup.Entities;
import javax.persistence.*;
import java.util.List;
@Entity
public class Card {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @OneToMany (fetch = FetchType.EAGER)
    private List<ChallengeClass> classList;

    public Card() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }


    public List<ChallengeClass> getClassList() {
        return classList;
    }

    public void setClassList(List<ChallengeClass> classList) {
        this.classList = classList;
    }

}
