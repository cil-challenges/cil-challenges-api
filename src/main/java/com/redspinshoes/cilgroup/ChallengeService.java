package com.redspinshoes.cilgroup;

import com.redspinshoes.cilgroup.Entities.Card;
import com.redspinshoes.cilgroup.Entities.ChallengeClass;
import com.redspinshoes.cilgroup.Entities.Participant;
import com.redspinshoes.cilgroup.Exceptions.MemberAlreadyExistsException;
import com.redspinshoes.cilgroup.Repositories.CardRepository;
import com.redspinshoes.cilgroup.Repositories.ChallengeClassRepository;
import com.redspinshoes.cilgroup.Repositories.ParticipantRepository;
import org.springframework.stereotype.Service;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class ChallengeService {

    private CardRepository cardRepository;
    private ParticipantRepository participantRepository;
    private ChallengeClassRepository challengeClassRepository;

    public ChallengeService(ParticipantRepository participantRepository, CardRepository cardRepository,
                            ChallengeClassRepository challengeClassRepository) {
        this.participantRepository = participantRepository;
        this.cardRepository = cardRepository;
        this.challengeClassRepository = challengeClassRepository;
    }

    public ArrayList<ChallengeClass> generateChallengeClasses(String cardType) {
        InstructorClasses classInstructors = new InstructorClasses();
        ArrayList<ChallengeClass> selectedClasses = new ArrayList<>();

        Instructor classInstructor;
        for (int i = 0; i < 12; i++) {
            int classNumCap = 1;
            if (cardType.equals("power zone")) {
                classNumCap = 3;
            }
            classInstructor = classInstructors.getInstructorPrimaryType(cardType,classNumCap);
            selectedClasses.add(new ChallengeClass(classInstructor.getInstructorName(), classInstructor.getClassType()));
        }
        for (int i = 12; i < 20 ; i++) {
            classInstructor = classInstructors.getInstructorPrimaryType("strength" ,1);
            selectedClasses.add(new ChallengeClass(classInstructor.getInstructorName(), classInstructor.getClassType()));
        }

        for (int i = 20; i < 25 ; i++) {
            classInstructor = classInstructors.getInstructorPrimaryType("yoga" ,1);
            selectedClasses.add(new ChallengeClass(classInstructor.getInstructorName(), classInstructor.getClassType()));
        }
        for (int i = 25; i < 30 ; i++) {
            selectedClasses.add(new ChallengeClass("Any Instructor", "Holiday"));
        }

        for (int i = 30; i < 35 ; i++) {
            classInstructor = classInstructors.getInstructorPrimaryType("meditation",1);
            selectedClasses.add(new ChallengeClass(classInstructor.getInstructorName(), classInstructor.getClassType()));
        }

        for (int i = 35; i < 43 ; i++) {
            classInstructor = classInstructors.getInstructorPrimaryType("stretch",1);
            selectedClasses.add(new ChallengeClass(classInstructor.getInstructorName(), classInstructor.getClassType()));
        }
        for (int i = 43; i < 45 ; i++) {
            classInstructor = classInstructors.getInstructorPrimaryType("other",1);
            selectedClasses.add(new ChallengeClass(classInstructor.getInstructorName(), "wild card"));
        }
        return selectedClasses;

    }

    public Participant addParticipant(Participant newParticipant) {

        Optional<Participant> oParticipant = participantRepository.findByLeaderboardName(newParticipant.getLeaderboardName());

        if (oParticipant.isPresent()) {
            throw new MemberAlreadyExistsException("A member with that memberId already exists");
        }

        Card participantCard = new Card();

        List<ChallengeClass> classList = generateChallengeClasses(newParticipant.getClassType());
        challengeClassRepository.saveAll(classList);

        participantCard.setClassList(classList);
        cardRepository.save(participantCard);

        newParticipant.setCard(participantCard);

        return participantRepository.save(newParticipant);
    }

    public ArrayList<BasicParticipantInfo> retrieveParticipants() {
        List<Participant> allParticipantInfo = new ArrayList<>(participantRepository.findAll());
        ArrayList<BasicParticipantInfo> allParticipantsBasic = new ArrayList<>();

        allParticipantInfo.forEach((participant) ->
                allParticipantsBasic.add(new BasicParticipantInfo(participant.getId(), participant.getLeaderboardName())));
        return allParticipantsBasic;
    }

    public Card getCard(String leaderboardName) {

        Optional<Participant> oParticipant = participantRepository.findByLeaderboardName(leaderboardName);

        if (oParticipant.isPresent()) {
            return oParticipant.get().getCard();
        }
        return null;
    }

    public Card updateCard(String leaderboardName, List<ChallengeClass> updatedClasses) {
        Optional<Participant> oParticipant = participantRepository.findByLeaderboardName(leaderboardName);

        if (oParticipant.isPresent()) {
            challengeClassRepository.saveAll(updatedClasses);
            oParticipant = participantRepository.findByLeaderboardName(leaderboardName);

            if(oParticipant.isPresent()) {
                return oParticipant.get().getCard();
            }
        }
        return null;
    }
}
