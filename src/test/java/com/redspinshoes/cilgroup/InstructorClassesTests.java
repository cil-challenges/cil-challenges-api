package com.redspinshoes.cilgroup;

import org.junit.jupiter.api.Test;

import java.util.Random;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

public class InstructorClassesTests {

    @Test
    void returnBikeInstructor() {
        InstructorClasses instructorClasses = new InstructorClasses();
        Instructor bikeClass = instructorClasses.getInstructorPrimaryType("bike", 2);
        assertEquals("bike", bikeClass.getClassType());
    }

    @Test
    void returnedBikeInstructorHasLessThan2Uses(){
        // arrange
        InstructorClasses instructorClasses = new InstructorClasses();
        Instructor[] instructorArray = instructorClasses.getInstructorClasses();

        for (int i = 0; i < instructorArray.length; i++) {
            if (instructorArray[i].getClassType().equals("bike")) {
                instructorArray[i].setUseCount(2);
            }
        }

        Random rand = new Random();
        int rand_int = 0;
        boolean foundInstructor = false;
        String saveInstructor = "";

        while (!foundInstructor) {
            rand_int = rand.nextInt(instructorArray.length);

            if (instructorArray[rand_int].getClassType().equals("bike")) {
                instructorArray[rand_int].setUseCount(1);
                saveInstructor = instructorArray[rand_int].getInstructorName();
                foundInstructor = true;
            }
        }

        // act
        Instructor bikeClass = instructorClasses.getInstructorPrimaryType("bike", 2);
        //assert
        assertEquals("bike", bikeClass.getClassType());
        assertEquals(saveInstructor, bikeClass.getInstructorName());
    }

    @Test
    void returnNonBikeInstructor() {
        InstructorClasses instructorClasses = new InstructorClasses();
        Instructor nonbikeClass = instructorClasses.getInstructorNonprimaryType("bike", 1);
        assertNotEquals("bike", nonbikeClass.getClassType());
    }

    @Test
    void returnNonBikeInstructorHasLessThan1Use(){
        // arrange
        InstructorClasses instructorClasses = new InstructorClasses();
        Instructor[] instructorArray = instructorClasses.getInstructorClasses();

        for (int i = 0; i < instructorArray.length; i++) {
            if (instructorArray[i].getClassType() != "bike") {
                instructorArray[i].setUseCount(1);
            }
        }

        Random rand = new Random();
        int rand_int = 0;
        boolean foundInstructor = false;
        String saveInstructor = "";

        while (!foundInstructor) {
            rand_int = rand.nextInt(instructorArray.length);

            if (instructorArray[rand_int].getClassType() != "bike") {
                instructorArray[rand_int].setUseCount(0);
                saveInstructor = instructorArray[rand_int].getInstructorName();
                foundInstructor = true;
            }
        }

        // act
        Instructor nonbikeClass = instructorClasses.getInstructorNonprimaryType("bike", 1);
        //assert
        assertNotEquals("bike", nonbikeClass.getClassType());
        assertEquals(saveInstructor, nonbikeClass.getInstructorName());
    }

    @Test
    void returnTreadInstructor() {
        InstructorClasses instructorClasses = new InstructorClasses();
        Instructor treadClass = instructorClasses.getInstructorPrimaryType("tread", 2);
        assertEquals("tread", treadClass.getClassType());
    }

    @Test
    void returnedTreadInstructorHasLessThan2Uses(){
        // arrange
        InstructorClasses instructorClasses = new InstructorClasses();
        Instructor[] instructorArray = instructorClasses.getInstructorClasses();

        for (int i = 0; i < instructorArray.length; i++) {
            if (instructorArray[i].getClassType().equals("tread")) {
                instructorArray[i].setUseCount(2);
            }
        }

        Random rand = new Random();
        int rand_int = 0;
        boolean foundInstructor = false;
        String saveInstructor = "";

        while (!foundInstructor) {
            rand_int = rand.nextInt(instructorArray.length);

            if (instructorArray[rand_int].getClassType().equals("tread")) {
                instructorArray[rand_int].setUseCount(1);
                saveInstructor = instructorArray[rand_int].getInstructorName();
                foundInstructor = true;
            }
        }

        // act
        Instructor treadClass = instructorClasses.getInstructorPrimaryType("tread", 2);
        //assert
        assertEquals("tread", treadClass.getClassType());
        assertEquals(saveInstructor, treadClass.getInstructorName());

    }

    @Test
    void returnNonTreadInstructor() {
        InstructorClasses instructorClasses = new InstructorClasses();
        Instructor nontreadClass = instructorClasses.getInstructorNonprimaryType("tread", 1);
        assertNotEquals("tread", nontreadClass.getClassType());
    }

    @Test
    void returnNonTreadInstructorHasLessThan1Use(){
        // arrange
        InstructorClasses instructorClasses = new InstructorClasses();
        Instructor[] instructorArray = instructorClasses.getInstructorClasses();

        for (int i = 0; i < instructorArray.length; i++) {
            if (instructorArray[i].getClassType() != "tread") {
                instructorArray[i].setUseCount(1);
            }
        }

        Random rand = new Random();
        int rand_int = 0;
        boolean foundInstructor = false;
        String saveInstructor = "";

        while (!foundInstructor) {
            rand_int = rand.nextInt(instructorArray.length);

            if (instructorArray[rand_int].getClassType() != "tread") {
                instructorArray[rand_int].setUseCount(0);
                saveInstructor = instructorArray[rand_int].getInstructorName();
                foundInstructor = true;
            }
        }

        // act
        Instructor nontreadClass = instructorClasses.getInstructorNonprimaryType("tread", 1);
        //assert
        assertNotEquals("tread", nontreadClass.getClassType());
        assertEquals(saveInstructor, nontreadClass.getInstructorName());
    }
}
